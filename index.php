<?php

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/':
        require __DIR__ . '/pages/manutenzione.php';
        break;
    case '':
        require __DIR__ . '/pages/manutenzione.php';
        break;
    case '/a':
        require __DIR__ . '/pages/index.php';
        break;
    case '/about':
        require __DIR__ . '/pages/about.php';
        break;
    case '/academy':
        require __DIR__ . '/pages/academy.php';
        break;
    case '/apprendistato':
        require __DIR__ . '/pages/apprendistato.php';
        break;
    case '/contatti':
        require __DIR__ . '/pages/contatti.php';
        break;
    case '/dopo-le-medie':
        require __DIR__ . '/pages/dopo-le-medie.php';
        break;
    case '/experienced':
        require __DIR__ . '/pages/experienced.php';
        break;
    case '/master':
        require __DIR__ . '/pages/master.php';
        break;
    case '/open-day':
        require __DIR__ . '/pages/open-day.php';
        break;
    case '/send-email':
        require __DIR__ . '/pages/helpers/send-email.php';
        break;
    default:
        require __DIR__ . '/pages/404.php';
        break;
}
