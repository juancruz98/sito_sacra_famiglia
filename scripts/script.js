// funzione per stretch testo con letter spacing dinamico

$.fn.strech_text = function() {
    var elmt = $(this),
        cont_width = elmt.width(),
        txt = elmt.text(),
        one_line = $('<span class="stretch_it">' + txt + '</span>'),
        nb_char = elmt.text().length,
        spacing = cont_width / nb_char,
        txt_width;

    elmt.html(one_line);
    txt_width = one_line.width();

    if (txt_width < cont_width) {
        var char_width = txt_width / nb_char,
            ltr_spacing = spacing - char_width + (spacing - char_width) / nb_char;

        one_line.css({
            'letter-spacing': ltr_spacing
        });
    } else {
        one_line.contents().unwrap();
        elmt.addClass('justify');
    }
};

$(document).ready(function() {
    $('.stretch').strech_text();
    $(window).resize(function() {
        $('.stretch').strech_text();
    });

    $('.third-button').on('click', function() {
        $('.animated-icon3').toggleClass('open');
    });
});


function sendForm(formID) {
    grecaptcha.ready(function() {
        grecaptcha.execute('6LeojOAZAAAAAADC3N1h84m2DGZTV2BXSRheTvby', { action: 'submit' }).then(function(token) {
            $(formID).find('.g-recaptcha-response').val(token);
            if (!$(formID)[0].checkValidity()) {
                $(formID)[0].reportValidity();

            } else {
                if (formID == '#contact-form') {
                    $.post('send-email', $(formID).serialize(), function(data) {
                            if (data == 'email mandata') {
                                $.toast({
                                    type: 'success',
                                    title: 'Email inviata correttamente!',
                                    content: 'Ti ricontatteremo presto.',
                                    delay: 5000,
                                });
                            } else {
                                $.toast({
                                    type: 'error',
                                    title: 'Errore!',
                                    content: 'Riprova.',
                                    delay: 5000,
                                });
                            }
                        },
                        'text'
                    );
                } else {
                    $.post('send-email', $(formID).serialize(), function(data) {
                            if (data == 'email mandata') {
                                $.toast({
                                    type: 'success',
                                    title: 'Grazie per esserti registrato alle nostre newsletter!',
                                    content: 'Novità presto in arrivo!',
                                    delay: 5000,
                                });
                            } else {
                                $.toast({
                                    type: 'error',
                                    title: 'Errore!',
                                    content: 'Riprova.',
                                    delay: 5000,
                                });
                            }
                        },
                        'text'
                    );
                }

            }
        });
    });
}