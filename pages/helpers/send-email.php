<?php
define("RECAPTCHA_V3_SECRET_KEY", '6LeojOAZAAAAANSnPusJ-EoVNdq2PmrVW7J1RWz3');
$errors = [];

if (!empty($_POST)) {
    $email = $_POST['email'];
    if ($_POST['form_type'] == 'contact') {
        $name = $_POST['nome-cognome'];
        $phone = $_POST['telefono'];
        $message = $_POST['messaggio'];
        if (empty($name)) {
            $errors[] = 'il campo Nome è vuoto';
        }
        if (empty($message)) {
            $errors[] = 'Il messaggio è vuoto';
        }

        if (empty($phone)) {
            $errors[] = 'Il messaggio è vuoto';
        }
    }
    if (empty($email)) {
        $errors[] = 'Il campo email è vuoto';
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'L\'email non è valida';
    }

    if (empty($errors)) {



        $token = $_POST['g-recaptcha-response'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => RECAPTCHA_V3_SECRET_KEY, 'response' => $token)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $arrResponse = json_decode($response, true);
        // verify the response
        if ($arrResponse["success"] == '1' && $arrResponse["score"] >= 0.5) {
            $toEmail = 'juancruzbarrionu98@gmail.com';
            $emailSubject = $_POST['oggetto_mail'];
            $headers = ['From' => $email, 'Reply-To' => $email, 'Content-type' => 'text/html; charset=iso-8859-1'];

            $bodyParagraphs = ["Email: {$email}"];
            if ($_POST['form_type'] == 'contact') {
                $bodyParagraphs[] = "Nome e Cognome: {$name}";
                $bodyParagraphs[] = "Telefono: {$phone}";
                $bodyParagraphs[] = "Messaggio: {$message}";
            }
            $body = join("<br>", $bodyParagraphs);
            if (mail($toEmail, $emailSubject, $body, $headers)) {
                echo "email mandata";
            } else {
                $errorMessage = 'Errore, email non mandata. Riprova';
            }
        } else {
            echo "non sei abbastanza umano, punteggio: " .  $arrResponse["score"];
        }
    } else {
        $allErrors = join('<br/>', $errors);
        $errorMessage = "<p style='color: red;'>{$allErrors}</p>";
        echo "errori nella form";
    }
}
