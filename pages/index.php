<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php include('blocks/nav.php'); ?>

    <div class="container-fluid gif-block-colors h-700">
        <div class="row h-100 justify-content-center relative-pos">
            <div class="grain-background"></div>
            <div class="col-12 col-md-10 col-lg-8 align-self-center text-center">
                <div>
                    <h1>l'importanza di una scelta</h1>
                    <div class="mt-5 wrap-btn">
                        <form action="dopo-le-medie">
                            <button type="submit" class="btn-trasparent button-anim black-hover font-20">scopri il nuovo
                                catalogo
                                corsi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('blocks/educazione.php'); ?>
    <div class="container-fluid gif-block-comonte">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-12 font-22" id="primo-blocco">
                <p>Siamo a<br>COMONTE di Seriate</p>
                <div class="wrap-btn">
                    <form action="open-day">
                        <button class="btn-white button-anim black-hover font-20" style="    
                    padding-left: 60px;
                    padding-right: 60px;
                    padding-top: 10px;
                    padding-bottom: 10px;
                    margin-top: 30px;">OPEN DAY</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include('blocks/virtual-tour.php'); ?>
    <?php include('blocks/colonne-colori.php'); ?>
    <div class="container-fluid black">
        <div class="row h-50 justify-content-center align-items-center pt-5 pb-5">
            <div class="col-10 col-md-4 col-lg-3 text-center">
                <div class="d-flex justify-content-around">
                    <a href="https://www.facebook.com/efpsacrafamiglia" target="_blank" rel="noopener noreferrer">
                        <img src="../images/facebook.png" class="circle-logo" alt="Logo Facebook">
                    </a>
                    <a href="https://www.instagram.com/efpsacrafamiglia/?hl=it" target="_blank" rel="noopener noreferrer">
                        <img src="../images/insta.png" class="circle-logo" alt="Logo Instagram">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgz6k1uxNE_hv3p0cNGfdVA" target="_blank" rel="noopener noreferrer">
                        <img src="../images/yt.png" class="circle-logo" alt="Logo YouTube">
                    </a>
                    <a href="https://it.linkedin.com/in/efp-sacra-famiglia-90a4551b3" target="_blank" rel="noopener noreferrer">
                        <img src="../images/linkedin.png" class="circle-logo" alt="Logo Linkedin">
                    </a>
                </div>

            </div>
        </div>
    </div>
    <?php include('blocks/offerta-formativa.php'); ?>
    <?php include('blocks/meeting-virtuale-full.php'); ?>
    <div class="container-fluid red">
        <div class="row justify-content-center">
            <div class="col-12 align-self-center text-center mt-100 mb-100">
                <p class="text-white l-spacing-2 font-22">EMERGENZA COVID</p>
                <div class="wrap-btn mt-5">
                    <button class="btn-white button-anim black-hover font-20" data-toggle="modal" data-target=".COVID-modal">info utili per la sicurezza di noi
                        tutti</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal COVID-->
    <div class="modal fade COVID-modal" tabindex="-1" role="dialog" aria-labelledby="COVID-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content p-5">
                <div class="modal-header">
                    <h4 class="modal-title">Protocollo Prevenzione
                        Covid</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <p><strong> ATTENZIONE! </strong>I nostri protocolli sono</span><strong> in fase di revisione </strong>in funzione della nuova Ordinanza FVG n.34 14/10/2020 contenente le &ldquo;Linee di indirizzo per la gestione dei contatti di casi confermati di Covid-.19 all&rsquo;interno delle scuole&rdquo;.</span></p>
                <p>Il Protocollo Prevenzione Covid di Sacra Famiglia prevede che l'</span><strong> accesso alle strutture Sacra Famiglia</strong> sia<strong> subordinato </strong>al sussistere di specifiche<strong> condizioni </strong>e prevede numerose iniziative per abbattere i rischi.</span></p>
                <h4>Norme generali</span></h4>
                <ul>
                    <li>E&rsquo;<strong> VIETATO </strong>accedere alla struttura con<strong> temperatura maggiore di 37,5&deg;C e/o </strong>in caso di presenza di<strong> uno o pi&ugrave; sintomi influenzali </strong>(tosse, mal di gola, raffreddore, cefalea, sintomi gastrointestinali, sintomi quali nausea vomito, diarrea, ecc.).<br /></span>In presenza della suddetta sintomatologia, il lavoratore/allievo &egrave; tenuto rivolgersi al medico curante (MMG) o pediatra di libera scelta (PLS) per la valutazione della situazione sanitaria;</span></li>
                    <li>Chiunque abbia la percezione di<strong> essere stato in contatto</strong> con persone<strong> positive </strong>al test del<strong> coronavirus</strong>, deve<strong> astenersi dal presentarsi presso l&rsquo;ente di formazione Sacra Famiglia</strong>, anche in assenza di sintomi. Dovr&agrave; rivolgersi al proprio MMG/PLS per eseguire gli accertamenti sanitari del caso e informare l&rsquo;ente di formazione Sacra Famiglia per attivare i protocolli di salvaguardia;</span></li>
                    <li>L&rsquo;</span><strong> accesso</strong> all&rsquo;ente di formazione Sacra Famiglia &egrave;<strong> consentito </strong>solo dopo essersi sottoposti al check-in di ingresso, unicamente nei varchi individuati e indossando la<strong> mascherina </strong>a protezione delle vie respiratorie. La stessa deve essere correttamente<strong> indossata </strong>per tutto il tempo di<strong> permanenza </strong>in struttura in tutte le situazioni in cui non fosse possibile garantire il distanziamento di almeno 1 metro;</span></li>
                    <li>Nel caso in cui una persona abbia<strong> percezione di essere stato in contatto con persone positive</strong> al test del coronavirus, questa<strong> non potr&agrave; presentarsi in struttura</strong>, anche in assenza di sintomi. Dovr&agrave; invece contattare il proprio MMG per eseguire gli accertamenti sanitari del caso e informare l&rsquo;ente di formazione Sacra Famiglia per attivare i protocolli di salvaguardia.</span></li>
                </ul>
                <h4>&Egrave; responsabilit&agrave; di ognuno:</span></h4>
                <ul>
                    <li>Mantenere le<strong> distanze </strong>di almeno 1 metro tra le persone sia in fase di ingresso sia in fase di permanenza nelle strutture;</span></li>
                    <li><strong> Lavarsi le mani</strong> con soluzione</span><strong> idroalcolica</strong> predisposta<strong> all&rsquo;ingresso</strong> e in vari punti delle strutture seguendo le indicazioni esposte. Il tempo minimo richiesto con utilizzo di soluzione idroalcolica &egrave; di 30-40 secondi. Nel caso di utilizzo del sapone il tempo minimo &egrave; di 40- 60 s.;</span></li>
                    <li>Durante la permanenza in struttura</span><strong> lavarsi spesso le mani</strong>;</span></li>
                    <li>Sottoporsi alla<strong> misurazione della febbre</strong> tramite termoscanner rapidi o termometro manuale.&nbsp; Le telecamere sono installate all'ingresso principale e nell&rsquo;ingresso posteriore;</span></li>
                    <li>Indossare correttamente, coprendo naso e bocca, le protezioni delle vie respiratorie (es.<strong> mascherina chirurgica</strong> tipo 3 veli o mascherina ffp2 o comunitaria) nei luoghi al chiuso e all&rsquo;esterno.</span><br /></span>&Egrave;<strong> obbligatorio usarla</strong> sempre nei seguenti casi:</span><br /></span>- durante gli spostamenti in struttura</span><br /></span>- nei corridoi</span><br /></span>- in co-presenza ravvicinata inferiore al metro</span><br /></span>- nei laboratori</span><br /></span>- nei servizi igienici</span></li>
                    <li>Prendere visione della cartellonistica esposta relativa ai comportamenti da seguire e rispettare quanto previsto;&nbsp;</span></li>
                    <li>Seguire e rispettare la segnaletica orizzontale e verticale;</span></li>
                    <li>Gettare eventuali mascherine usate negli appositi contenitori.</span></li>
                </ul>
                <h4>&nbsp;</span></h4>
                <h4>Comportamento in aula</span></h4>
                <ul>
                    <li>Le aule sono predisposte affinch&eacute; sia mantenuto il distanziamento tra postazioni fisse;</span></li>
                    <li>Gli allievi dovranno prendere posto nel banco assegnato e solo mantenendo il distanziamento potranno abbassare la mascherina;</span></li>
                    <li>La mascherina va sempre utilizzata nel momento in cui ci sia la necessit&agrave; di alzarsi dal banco e/o muoversi all&rsquo;interno dell&rsquo;aula e/o non sia rispettato il metro di distanza;</span></li>
                    <li>Il docente &egrave; tenuto a mantenere una distanza di almeno 2 metri dagli allievi e potr&agrave; utilizzare la visiera protettiva;</span></li>
                    <li><strong> Non &egrave; consigliato</strong> lo<strong> scambio di attrezzature personali</strong> (matite, penne, gomme ecc.) tra gli allievi</span></li>
                    <li>Il computer d&rsquo;aula &egrave; ad esclusivo dell&rsquo;insegnante e non pu&ograve; essere utilizzato dagli allievi;</span></li>
                    <li>Le interrogazioni potranno essere svolte o dal posto o nel caso in cui ci sia la necessit&agrave; di utilizzo della lavagna l&rsquo;allievo dovr&agrave; indossare obbligatoriamente la mascherina e il docente la visiera e/o mascherina;</span></li>
                </ul>
                <p>Si raccomanda di<strong> arieggiare </strong>spesso le aule e in particolare tra una ora e l&rsquo;altra le finestre vanno sempre aperte.&nbsp;</span></p>
                <p>Prima di lasciare l&rsquo;aula si chiede la</span><strong> collaborazione fattiva</strong> dei ragazzi al fine di<strong> sanificare </strong>il banco/postazione con spray idonei e forniti in dotazione dall&rsquo;ente di formazione Sacra Famiglia.</span></p>
                <h4>&nbsp;</span></h4>
                <h4>Comportamento in laboratorio</span></h4>
                <p>Nei laboratori &egrave;<strong> obbligatorio utilizzare la mascherina di tipo chirurgico</strong> oltre ai normali dispositivi di protezione individuali previsti per la mansione.</span></p>
                <p>&nbsp;</span></p>
                <h4>Pause e ricreazioni</span></h4>
                <ul>
                    <li>&Egrave; possibile accedere ai distributori automatici mantenendo sempre la distanza di sicurezza e indossando la<strong> mascherina</strong>;</span></li>
                    <li><strong> Usare esclusivamente i distributori del proprio piano</strong>;</span></li>
                    <li>Raggiungere l'area distributori snack e bevande al massimo in 2-3 persone e sempre con la mascherina;</span></li>
                    <li><strong> Lavarsi </strong>le mani</span><strong> prima e dopo la selezione e la consumazione</strong>, utilizzando il dispenser predisposto.</span></li>
                </ul>
                <p>&nbsp;</span></p>
                <h4>Come comportarsi in caso di sintomi&nbsp;</span></h4>
                <p>Abbiamo predisposto un riepilogo con le indicazioni da seguire in caso di presenza di sintomi riconducibili a Covid-19.</span></p>
                <p>Tutte le norme comportamentali sono riportate anche nel</span><strong> Patto di Corresponsabilit&agrave;</strong> inviato a tutte le Famiglie con richiesta di sottoscrizione.</span></p>
                <h4>Come comportarsi dopo un'assenza: autocertificazione per il rientro a scuola</span></h4>
                <p>Abbiamo predisposto un agile riepilogo con tutte le casistiche di assenza e le relative procedure e regole comportamentali cui attenersi.&nbsp;</span></p>
                <p>Consulta la<strong> segreteria </strong>per avere maggiori informazioni a riguardo.</span></p>
                <p>&nbsp;</span></p>
                <h4>Attenzione ai soggetti fragili</span></h4>
                <p>Abbiamo predisposto un iter aggiuntivo di sorveglianza sanitaria eccezionale per i lavoratori e allievi maggiormente a rischio contagio, in accordo con il Medico competente.</span></p>
                <p><br /><br /></p>
                <div class="modal-footer font-20">
                    <a data-dismiss="modal">Chiudi</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Fine Modal -->
    <!-- <div class="container-fluid ">
        <div class="row justify-content-center">
            <div class="col-12 align-self-center text-center mt-100 mb-100">
                <p class="l-spacing-2 font-22">NEWS</p>
            </div>
        </div>
    </div> -->
    <?php include('blocks/newsletter-gif.php'); ?>
    <?php include('blocks/candidatura.php'); ?>
    <?php include('blocks/footer.php'); ?>

</body>

</html>