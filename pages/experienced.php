<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'progetti';
    include('blocks/nav.php');
    ?>
    <?php include('blocks/sezione-experienced.php'); ?>
    <?php include('blocks/colonne-colori.php'); ?>
    <?php include('blocks/footer.php'); ?>
</body>

</html>