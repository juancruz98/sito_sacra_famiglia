<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'contatti';
    include('blocks/nav.php');
    ?>
    <!-- toast -->
    <div class="toast toast-success" data-autohide="true" style="position: absolute;top: 70; right: 0; z-index:1; margin-right:15px;  margin-top:15px;" data-delay="4000">
        <div class="toast-header green">
            <strong class="mr-auto text-white">Email inviata correttamente!</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Ti ricontatteremo presto.
        </div>
    </div>
    <div class="toast toast-fail" data-autohide="true" style="position: absolute;top: 70; right: 0; z-index:1; margin-right:15px;  margin-top:15px;" data-delay="4000">
        <div class="toast-header red">
            <strong class="mr-auto text-white">Oops!</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            C'è stato un errore! Riprova.
        </div>
    </div>
    <!--  -->
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center pt-50 pb-50">
            <div class="col-12 col-lg-5">
                <div class="embed-responsive embed-responsive-4by3" style="filter: grayscale(1);">
                    <iframe class="embed-responsive-item" src="https://maps.google.com/maps?q=EFP%20Sacra%20Famiglia%20Via%20Luigia%20Corti%2C9%20Seriate%2C%20Bergamo%2C%20Italy&amp;t=m&amp;z=15&amp;output=embed&amp;iwloc=near" title="EFP Sacra Famiglia Via Luigia Corti,9 Seriate, Bergamo, Italia" aria-label="EFP Sacra Famiglia Via Luigia Corti,9 Seriate, Bergamo, Italia" width="100%" height="650px" frameborder="0" style="border:0; display: block;" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-12 col-lg-5 mt-4 mt-lg-0 h-100">
                <h1 class="title">Contattaci</h1>
                <form id="contact-form" method="POST">
                    <div class="form-group">
                        <label for="nome-cognome">Nome e Cognome</label>
                        <input type="text" class="form-control" name="nome-cognome" placeholder="Nome e Cognome" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="telefono">Telefono</label>
                        <input type="text" class="form-control" name="telefono" placeholder="Telefono">
                    </div>
                    <div class="form-group">
                        <label for="messaggio">Messaggio</label>
                        <textarea class="form-control" name="messaggio" rows="3" required></textarea>
                    </div>
                    <input type="hidden" name="oggetto_mail" value="Nuova email da form di contatto sito Sacra Famiglia">
                    <input type="hidden" name="form_type" value="contact">
                    <input type="hidden" class="g-recaptcha-response" name="g-recaptcha-response">
                    <div class="wrap-right-btn">
                        <button type="button" class="g-recaptcha btn-red button-anim aquamarine-hover font-20 px-5 float-right font-akzidenz" onclick="sendForm('#contact-form')">INVIA</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <?php include('blocks/newsletter-gif.php'); ?>
    <?php include('blocks/candidatura.php'); ?>
    <?php include('blocks/footer.php'); ?>
</body>

</html>