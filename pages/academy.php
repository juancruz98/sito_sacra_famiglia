<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'progetti';
    include('blocks/nav.php');
    ?>
    <div class="container-fluid grey pt-100 pb-100">
        <div class="row h-100 justify-content-center">
            <div class="col-10 col-md-6 col-lg-5 align-self-center text-center">
                <h1 class="text-white">academy</h1>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/CTw9N63TYso" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <div class="text-justify">
                    <p>
                        Un luogo fisico e virtuale in cui si condividono e sviluppano conoscenze, saperi, tecniche, pratiche e idee innovative per accrescere i nostri ragazzi.
                    </p>
                    <p>
                        3 Aziende rappresentative tra Bergamo e Milano ospitano i ragazzi con l’obiettivo di garantire lo sviluppo del capitale umano e di tramandare le diverse professionalità, con progetti, contenuti e modalità formative diversificate in tre macro aree: Comunicazione e produzione multimediale, web marketing e incubatore d’impresa agricola.
                    </p>
                    </p>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid grey pt-100 pb-100">
        <div class="row h-100 justify-content-center">
            <div class="col-12 col-md-4 text-center">
                <img src="../images/SF_Foto_academy" alt="" class="img-fluid mb-5 mb-lg-0">
            </div>
            <div class="col-12 col-md-4  text-center">
                <img src="../images/SF_Foto_academy_1" alt="" class="img-fluid mb-5 mb-lg-0">
            </div>
            <div class="col-12 col-md-4  text-center">
                <img src="../images/SF_Foto_academy_2" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <?php include('blocks/colonne-colori.php'); ?>
    <?php include('blocks/newsletter-gif.php'); ?>
    
    <?php include('blocks/footer.php'); ?>
</body>

</html>