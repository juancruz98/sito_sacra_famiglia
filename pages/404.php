<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'corsi';
    include('blocks/nav.php');
    ?>
    <div class="container-fluid gif-block-colors h-700">
        <div class="row h-100 justify-content-center">
            <div class="col-10 col-md-6 col-lg-5 align-self-center text-center">
            <h1>OOPS! Pagina non trovata</h1>
            </div>
        
        </div>
    </div>

    <?php include('blocks/footer.php'); ?>
</body>

</html>