<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'corsi';
    include('blocks/nav.php');
    ?>
    <div class="container-fluid font-22">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <div class="stretch-container">
                    <h3 class="stretch">Formiamo alla responsabilità</h3>
                </div>
                <br>
                <div class="text-justify">
                    <p>
                        I corsi di formazione Sacra Famiglia si compongono di teoria e pratica: in aula apprendi ciò che
                        ti
                        servirà sul campo; in laboratorio impari a utilizzare gli strumenti del mestiere.
                    </p>
                    <p>
                        Con gli stage in azienda, entri in contatto con i professionisti del settore scelto e, alla fine
                        dei 3 anni, consegui
                        una qualifica professionale con cui puoi lavorare in tutt'Europa.
                    </p>
                    <p>
                        Frequenza e iscrizione sono <b>gratuite</b>: i corsi sono approvati e finanziati dalla Regione
                        Lombardia.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Sezione Commerciale -->
    <div class="container-fluid orange h-700" id="sezione_commerciale">
        <div class="row h-100 justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Operatore addetto ai servizi di Vendita</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/HB9ZjBQGoew" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <p class="text-justify">L’Operatore Commerciale ai servizi di vendita interviene a livello esecutivo,
                    nel processo della distribuzione commerciale. In particolare si conseguono abilità specifiche nello
                    svolgimento delle operazioni fondamentali attinenti all’organizzazione del punto vendita, alla cura
                    e al servizio di vendita e post vendita, con l’acquisizione di competenze concernenti gli
                    adempimenti amministrativi basilari, l’organizzazione degli ambienti e degli spazi espositivi nella
                    predisposizione di iniziative promozionali. Il curriculum formativo è fortemente orientato
                    all’inserimento all’interno di attività commerciali di piccole e grandi dimensioni nel settore del
                    food e del non food.I ragazzi sono accompagnati nella stesura di un piano di lavoro e di un
                    curriculum vitae in previsione dell’inserimento lavorativo. L’indirizzo offre anche la possibilità
                    di proseguire con un quarto anno di formazione tecnico professionale.</p>
                <div class="wrap-btn mt-5 mb-3">
                    <a href="../documents/SCHEDA_Commerciale.pdf" target="_blank" download="scheda_commerciale">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">Scarica il
                            programma
                            :D</button>
                    </a>
                </div>
                <div class="wrap-btn mt-2">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pt-50 pb-50 dark font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-10 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="col-12 mx-auto mb-2">
            <?php include("blocks/sub-blocks/burgo.php") ?>
        </div>
    </div>
    <div class="container-fluid font-22 orange">
        <div class="row justify-content-center align-items-center">
            <div class="col-10 align-self-center text-center mt-50 mb-50 text-white">
                <p class="m-0">Per chi prosegue con il IV anno</p>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <p class="text-justify">Il Tecnico Commerciale delle Vendite interviene con autonomia, nel quadro di
                    azione stabilito e delle specifiche assegnate, contribuendo al presidio del processo di
                    distribuzione commerciale attraverso la partecipazione all’individuazione delle risorse, il
                    monitoraggio e la valutazione del risultato, l’implementazione di procedure di miglioramento
                    continuo, con assunzione di responsabilità relative alla sorveglianza di attività esecutive svolte
                    da altri. La formazione tecnica nell’utilizzo di metodologie, strumenti e informazioni specializzate
                    gli consente di svolgere attività relative agli ambiti della predisposizione e dell’organizzazione
                    del punto vendita, con competenze relative alla realizzazione del piano di acquisti,
                    all’amministrazione d’esercizio ed alla gestione dei rapporti con il cliente.</p>
                <div class="wrap-btn mt-5 mb-3">
                    <a href="../documents/SCHEDA_Tecnici.pdf" target="_blank" download="scheda_tecnici">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">Scarica il
                            programma
                            :D</button>
                    </a>
                </div>
                <div class="wrap-btn mt-2">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- video IV anno commerciale -->
    <div class="container-fluid orange h-700">
        <div class="row h-100 justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Tecnici IV Anno</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/nHm8EQG5Jyo" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    <div class="container-fluid pt-50 pb-50 dark font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-10 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="col-12 mx-auto mb-2">
            <?php include("blocks/sub-blocks/vitali.php") ?>
        </div>
    </div>
    <!-- End Sezione Commerciale -->

    <!-- Sezione Agricolo -->
    <div class="container-fluid green h-700" id="sezione_agricolo">
        <div class="row h-100 justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Operatore Agricolo</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/3yVCbgUTpOA" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <p class="text-justify">L’Operatore Agricolo interviene come specialista a tutela del territorio e del
                    paesaggio, ponendo un’attenzione particolare e un preciso impegno nella salvaguardia e nella
                    creazione di un futuro green per il nostro territorio. Grazie all’applicazione di metodologie di
                    base, di strumenti e informazioni specifiche si acquisiscono competenze e conoscenze che consentono
                    di collaborare alla gestione dell’azienda in termini di riscoperta delle tradizioni nell’ottica
                    della modernità. In particolare si conseguono abilità specifiche nello svolgimento delle operazioni
                    fondamentali attinenti alla progettazione, alla creazione, alla cura e alla manutenzione del terreno
                    nell’ambito della tutela e protezione dell’ambiente naturale. Il curriculum formativo è fortemente
                    orientato al green-job, ormai nuova frontiera del mercato del lavoro, approfondendo ogni aspetto
                    della professione agricola, dalla produzione zootecnica e vegetale, all’esecuzione di operazioni
                    nella trasformazione di prodotti primari dell’azienda.</p>
                <div class="wrap-btn mt-5 mb-3">
                    <a href="../documents/SCHEDA_Agricolo.pdf" target="_blank" download="scheda_agricolo">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">Scarica il
                            programma
                            :D</button>
                    </a>
                </div>
                <div class="wrap-btn mt-2">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-50 pb-50 dark font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-10 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="col-12 mx-auto mb-2">
            <?php include("blocks/sub-blocks/capelli.php") ?>
        </div>
    </div>
    <div class="container-fluid font-22 green">
        <div class="row justify-content-center align-items-center">
            <div class="col-10 align-self-center text-center mt-50 mb-50 text-white">
                <p class="m-0">Per chi prosegue con il IV anno</p>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <p class="text-justify">Il Tecnico Agricolo interviene con autonomia nell’ambito dei processi
                    gestionali, aziendali e produttivi attraverso la partecipazione all’individuazione delle risorse e
                    delle possibilità di sviluppo dell’azienda agricola. Opera a seconda del contesto
                    territoriale/produttivo di riferimento, dell’organizzazione operativa, dell’implementazione di
                    procedure di miglioramento continuo, del monitoraggio e della valutazione del risultato, con
                    assunzione di responsabilità concernenti il coordinamento di attività esecutive svolte da altri. La
                    formazione tecnica nell’utilizzo di metodologie, strumenti e informazioni specializzate, rispetto
                    alla coltivazione arborea e/o erbacea e/o ortofloricola, gli consente di svolgere attività relative:
                    alla gestione dell’azienda agricola, con competenze funzionali alla scelta degli indirizzi
                    produttivi, degli investimenti e delle filiere di commercializzazione nonché alla relazione con i
                    fornitori, ai fatti amministrativo contabili, alla programmazione, organizzazione/sorveglianza delle
                    fasi di lavoro e alla valutazione del processo/prodotto.</p>
                <div class="wrap-btn mt-5 mb-3">
                    <a href="../documents/SCHEDA_Tecnici.pdf" target="_blank" download="scheda_tecnici">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">Scarica il
                            programma
                            :D</button>
                    </a>
                </div>
                <div class="wrap-btn mt-2">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- video IV anno commerciale -->
    <div class="container-fluid green h-700">
        <div class="row h-100 justify-content-center">
        <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Tecnici IV Anno</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/nHm8EQG5Jyo" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    <div class="container-fluid pt-50 pb-50 dark font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-10 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="col-12 mx-auto mb-2">
            <?php include("blocks/sub-blocks/vitali.php") ?>
        </div>
    </div>
    <!-- Sezione Agricolo -->

    <!-- Sezione Abbigliamento -->
    <div class="container-fluid purple h-700">
        <div class="row h-100 justify-content-center">
        <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Operatore dell'Abbigliamento</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/xwa1szPoquI" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <p class="text-justify">L’Operatore dell’Abbigliamento, interviene, a livello esecutivo, nel processo di produzione tessile e
                    abbigliamento con autonomia e responsabilità limitate a ciò che prevedono le procedure e le metodiche della sua operatività.
                    La qualificazione nell’applicazione/utilizzo di metodologie di base, di strumenti e di informazioni gli consentono di svolgere
                    attività con competenze relative alla realizzazione di figurini e modelli, all’esecuzione delle operazioni di taglio, all’assemblaggio
                    e confezionamento del prodotto.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid font-22 purple">
        <div class="row justify-content-center align-items-center">
            <div class="col-10 align-self-center text-center mt-50 mb-50 text-white">
                <div class="wrap-btn">
                    <a href="https://feelgood.efpsacrafamiglia.com" target="_blank">
                        <button class="btn-white button-anim black-hover font-20">Visita il sito ufficiale Feel
                            Good</button></a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Sezione Abbigliamento -->
    <!-- Sezione Personalizzazione -->
    <div class="container-fluid grey h-700" id="personalizzazione">
        <div class="row h-100 justify-content-center">
        <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Area Personalizzazione</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/9SAPtNpD--g" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <div class="text-justify">
                    <p>
                        Personalizzare il percorso formativo significa attivare buone prassi che tengano
                        conto dei punti di forza e delle fragilità di ciascuno.
                    </p>
                    <p>
                        Personalizzare consiste nel costruire un’opportunità formativa capace di valorizzare ciascuno
                        senza
                        creare illusioni o all’opposto costruire un percorso che limiti le reali potenzialità
                        dell’allievo/a.
                    </p>
                    <p>
                        Personalizzare è un lavoro di concerto tra i formatori, gli alunni e le famiglie.
                    </p>
                    <p>
                        Personalizzare implica impostare un lavoro che educhi alla diversità, letta come un valore
                        aggiunto
                        e non solo come un limite.
                    </p>
                    <p>
                        Personalizzare significa sapersi avvicinare all’altro senza avere la pretesa che sia lui ad
                        adeguarsi a come funziono io. È un lavoro di mediazione che permette un incontro che ha come
                        obiettivo il “costruire” la professionalità di ciascuno.
                    </p>
                    <p>
                        Personalizzare è far acquisire competenze relazionali, culturali e professionali spendibili in
                        ambito lavorativo nonostante le fragilità che fanno parte di ciascuno di noi.
                    </p>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid pt-50 pb-50 grey font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-10 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="col-12 mx-auto mb-2">
            <?php include("blocks/sub-blocks/piersanti.php") ?>
        </div>
    </div>
    <!-- End Sezione Personalizzazione -->
    <?php include('blocks/faq.php'); ?>
    <?php include('blocks/footer.php'); ?>
</body>

</html>