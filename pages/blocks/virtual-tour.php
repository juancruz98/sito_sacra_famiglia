<div class="font-22 dark text-white no-gutters vt-block" id="sede">
    <div class="row justify-content-center no-gutters">
        <div class="col-8 col-sm-6 col-lg-4 align-self-center text-center mt-100 mb-100">
            Esplora i nostri spazi
        </div>
    </div>
    <iframe width="100%" scrolling="no" id="iframe-360" src="https://poly.google.com/view/8q7yFq6CtCh/embed?chrome=min" frameborder="0" style="border:none;" allowvr="yes" allow="accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>
</div>

<?php if (isset($light_theme)) : ?>
    <style>
        .vt-block {
            background-color: white;
            color: black !important;
        }
    </style>
<?php endif ?>