<div class="d-flex justify-content-center">
    <div class="img-container">
        <a href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/sacra-famiglia/appuntamento-privato'});return false;">
            <img src="../images/Sacra_famiglia_SF_donadoni.png" width="170px" height="170px" class="center-block image-coordinatore">
            <span class="center-text-over-image text-white">PRENOTA</span>
        </a>
    </div>
</div>
<h5 class="text-center title-meetings mt-2">Simone Donadoni</h5>
<p class="text-center subtitle-meetings">Servizi al Lavoro</p>