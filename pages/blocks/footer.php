<footer class="container">
    <div class="row pt-5 mb-2  pl-5">
        <div class="col-12">
            <img src="../images/logo_nero.png" height="50" class="" alt="">
        </div>
    </div>
    <div class="row mt-5 pl-5">
        <div class="col-12 col-md-3 pl-0 pl-lg-5 my-auto f-links">
            <div class="mb-3">
                <a href="dopo-le-medie">Formazione per ragazzi</a>
            </div>
            <div class="mb-3">
                <a href="master" target="_blank">Corso per disoccupati</a>
            </div>
            <div class="mb-3">
                <a href="apprendistato">Formazione permanente</a>
            </div>
        </div>
        <div class="col-12 col-md-3  pl-0 pl-lg-5 my-auto f-links">
            <div class="mb-3">
                Bilancio Sociale
            </div>
            <div class="mb-3">
                <a href="#" data-toggle="modal" data-target="#info_modal">Info ex L.124/17 art.1 co.125</a>
            </div>
            <div class="mb-3">
                <a href="mailto:segreteriabg@efpsacrafamiglia.it">Lavora con noi</a>
            </div>
        </div>
        <!-- Modal  -->
        <div class="modal fade" tabindex="-1" role="dialog" id="info_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Info ex L.124/17 art.1 co.125</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>In adempimento agli obblighi di trasparenza di cui all’ art. 1, commi 125 e ss. della Legge 4 agosto 2017 n. 124, si comunica che per l’anno 2019 le informazioni richieste dalle citate disposizioni normative sono state pubblicate nella Nota integrativa al Bilancio chiuso al 31/08/2019 consultabile presso il Registro delle Imprese, così come chiarito dal Ministero del Lavoro nella circolare della Direzione Generale per il Terzo settore dell’11 gennaio 2019, n. 2.
                            In adempimento agli obblighi di trasparenza di cui all’ art. 1, commi 125 e ss. della Legge 4 agosto 2017 n. 124, si comunica che per l’anno 2019 le informazioni richieste dalle citate disposizioni normative sono state pubblicate nella Nota integrativa al Bilancio chiuso al 31/08/2019 consultabile presso il Registro delle Imprese, così come chiarito dal Ministero del Lavoro nella circolare della Direzione Generale per il Terzo settore dell’11 gennaio 2019, n. 2.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal">Chiudi</a>
                    </div>
                </div>
            </div>
        </div>
        <!--  -->
        <div class="col-12 col-md-6  pl-0 pl-lg-5 mt-3 mt-md-0 f-contatti">
            <h3>Contatti</h3>
            <p class="font-weight-bold">ENTE DI FORMAZIONE SACRA FAMIGLIA</p>
            <p>Ente Accreditato Sezione A Servizi al Lavoro ID OPERATORE 1103775 <a href="https://www.google.com/maps/place/EFP+Sacra+Famiglia/@45.6847515,9.748298,17z/data=!4m5!3m4!1s0x47815a677e62b37b:0xd041590b132468ea!8m2!3d45.684748!4d9.7504869" target="_blank" rel="noopener noreferrer"> Via Luigia Corti n.9, 24068 Seriate
                    BG</a>
            </p>
            <p>CF/PIVA: 03723100164</p>
            <p><b>T:</b><a href="tel:+39035302686"> 035 302686</a> <b>E:</b> <a href="mailto:segreteriabg@efpsacrafamiglia.it"> segreteriabg@efpsacrafamiglia.it</a></p>
            <p><b>T:</b><a href="tel:+390352922824"> 035 2922824</a> <b>E:</b> <a href="mailto:serviziallavoro@efpsacrafamiglia.it"> serviziallavoro@efpsacrafamiglia.it</a></p>
        </div>
    </div>
    <div class="row mb-5 mt-5 justify-content-center">
        <div class="col-10 col-md-6">
            <div class="d-flex justify-content-center">
                <a href="https://www.regione.lombardia.it/wps/portal/istituzionale/" target="_blank" rel="noopener noreferrer">
                    <img src="../images/banner_footer.png" style="max-width: 100%; object-fit: contain;" alt="">
                </a>
            </div>
        </div>
    </div>

</footer>

<link rel="stylesheet" type="text/css" href="https://cdn.wpcc.io/lib/1.0.2/cookieconsent.min.css" />
<script src="https://cdn.wpcc.io/lib/1.0.2/cookieconsent.min.js" defer></script>
<script>
    window.addEventListener("load", function() {
        window.wpcc.init({
            "border": "thin",
            "corners": "small",
            "colors": {
                "popup": {
                    "background": "#222222",
                    "text": "#ffffff",
                    "border": "#ffffff"
                },
                "button": {
                    "background": "#ffffff",
                    "text": "#000000"
                }
            },
            "position": "bottom",
            "content": {
                "message": "Questo sito web utilizza i cookie.",
                "link": "Ulteriori informazioni",
                "button": "Accetto",
                "href": "https://www.iubenda.com/privacy-policy/78792902"
            }
        })
    });
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LeojOAZAAAAAADC3N1h84m2DGZTV2BXSRheTvby"></script>
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
<script src="../scripts/toast.js"></script>
<script src="../scripts/script.js"></script>