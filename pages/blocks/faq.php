<div class="container">
        <div class="row justify-content-center">
            <div class="col-12 align-self-center mt-100 mb-100">
                <h2 class="text-center">FAQ</h2>
                <div id="accordion">
                    <div class="card">
                        <div class="card-header bg-white" id="headingOne">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Qual’è la proposta formativa dell’EFP Sacra Famiglia?
                                </span>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>
                                    Un Team di Professionisti qualificati ti guiderà in ogni passo del tuo percorso. Istruzione e lavoro coniugati per dar forma ad un’esperienza formativa innovativa. Due indirizzi: Operatore Addetto ai Servizi di Vendita e Operatore Agricolo, con la possibilità
                                    di proseguire con un quarto anno per ottenere la qualifica di Tecnico Livello europeo 4EQF. Con questo titolo è possibile accedere a successivi percorsi di specializzazione (IFTS) ed eventualmente a percorsi di ITS
                                    (Istruzione Tecnica Superiore) o accedere al quinto anno dell’istruzione per ottenere il Titolo di Stato.
                                </p>
                                <p>
                                    L’accesso alle aziende sul territorio è garantito per stage e tirocini al fine di sviluppare iniziativa e imprenditorialità.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingTwo">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Mission dell’EFP Sacra Famiglia
                                </span>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                La Mission dell’ente è quella di motivare lo studente e aiutandolo a far emergere le sue capacità per dare il meglio di sé per realizzarsi: infatti i corsi non si focalizzano unicamente sull’apprendimento ma garantiscono l’acquisizione di un metodo di
                                lavoro. La figura del tutor non si sostituisce allo studente, ma fornisce gli strumenti necessari per poter raggiungere i traguardi prefissati. Lo studente in difficoltà trova all’EFP Sacra Famiglia un team alla personalizzazione
                                estremamente preparato in grado di fornire un metodo necessario a completare con successo il proprio percorso formativo fino al conseguimento del titolo di studio.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingThree">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Visione dell’Ente
                                </span>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                La struttura, moderna e dinamica, offre numerose proposte didattiche grazie ad un’ampia gamma di corsi in grado di accontentare ogni singola attitudine e preferenza scolastica. La collaborazione con insegnanti qualificati e il rispetto di programmi personalizzati
                                e flessibili, inoltre, permette agli studenti di trovare motivazione, supporto e competenza nel loro percorso, ottenendo buoni risultati senza rinunciare agli impegni di lavoro.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingFour">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Il Team dell’EFP Sacra Famiglia
                                </span>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Il Team si compone di formatori selezionati e formati, in continuo aggiornamento e provenienti da realtà professionali del territorio.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingFive">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Orario Scolastico
                                </span>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Le lezioni si tengono dal lunedì al venerdì, a partire dalle 8 alle 14. Saltuariamente sono previsti incontri pomeridiani dedicati al recupero.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingSix">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    La Scuola è a pagamento?
                                </span>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                            <div class="card-body">
                                No, la rata scolastica è coperta dal sistema Doti di Regione Lombardia. È richiesta un esborso per l’acquisto di un personal computer al quarto anno, fondamentale per alcuni corsi.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingSeven">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    Stage
                                </span>
                            </h5>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                            <div class="card-body">
                                EFP Sacra famiglia si impegna ogni anno ad allargare la rete aziende per consentire ad ogni suo studente un percorso di tirocinio personalizzato e su misura. Attualmente collaboriamo con più di 150 Aziende sul territorio.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white" id="headingEight">
                            <h5 class="mb-0">
                                <span data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                    Internazionalizzazione
                                </span>
                            </h5>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                            <div class="card-body">
                                Sono previste Learning Week all’estero o esperienze lavorative in aziende qualificate in alcune delle più appetibili città europee. Questo da modo ai ragazzi di sperimentare esperienze formative di alto livello a fronte di un mercato sempre più globale.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>