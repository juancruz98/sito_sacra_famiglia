<div class="container-fluid pt-50 pb-50 dark font-22 text-white">
    <div class="row h-50 justify-content-center align-items-center">
        <div class="col-11 col-sm-6 align-self-center mb-5" id="meeting">
            <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
        </div>
    </div>
    <div class="container justify-content-center">
        <div class=" row justify-content-center h-50">
            <div class="col-12 col-sm-4 col-xl-2 mx-xl-auto mb-2">
                <?php include("sub-blocks/burgo.php") ?>
            </div>
            <div class="col-12 col-sm-4 col-xl-2 mx-xl-auto mb-2">
                <?php include("sub-blocks/capelli.php") ?>
            </div>
            <div class="col-12 col-sm-4 col-xl-2 mx-xl-auto mb-2">
                <?php include("sub-blocks/vitali.php") ?>
            </div>
            <div class="col-12 col-sm-4 col-lg-4 col-xl-2 mx-xl-auto mb-2">
                <?php include("sub-blocks/piersanti.php") ?>
            </div>
            <div class="col-12 col-sm-4 col-lg-4 col-xl-2 mx-xl-auto mb-2">
                <?php include("sub-blocks/donadoni.php") ?>
            </div>
        </div>
    </div>
</div>