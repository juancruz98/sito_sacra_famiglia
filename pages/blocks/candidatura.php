<div class="container-fluid py-5 dark">
    <div class="row h-100 justify-content-center relative-pos">
        <div class=" col-11 col-md-8 align-self-center text-center text-white">
            <p class="l-spacing-2 font-22">Sei interessato a collaborare con noi?</p>
            <div class="wrap-btn mt-3">
                <a href="mailto:segreteriabg@efpsacrafamiglia.it">
                    <button class="btn btn-blue button-anim white-hover font-20">INVIA LA TUA CANDIDATURA</button>
                </a>
            </div>
        </div>
    </div>
</div>