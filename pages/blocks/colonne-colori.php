<div class="container-fluid colors">
    <div class="row h-100 box-wrap">

        <div class="col-12 col-md-6 col-lg-3 orange box">
            <a href="dopo-le-medie#sezione_commerciale">
                <div class="d-flex justify-content-center h-100">
                    <div class="p-2 align-self-center">Commerciale</div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-6 col-lg-3 green box">
            <a href="dopo-le-medie#sezione_agricolo">
                <div class="d-flex justify-content-center h-100">
                    <div class="p-2 align-self-center">Agricolo</div>
                </div>
            </a>
        </div>

        <div class="col-12 col-md-6 col-lg-3 purple box">
            <a href="https://feelgood.efpsacrafamiglia.com" target="_blank">
                <div class="d-flex justify-content-center h-100">
                    <div class="p-2 align-self-center">Sartoria</div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-6 col-lg-3 red box">
            <a href="https://www.landpage.co/56c1a2fc-a19c-11ea-9280-f2105ecc4ec9" target="_blank">
                <div class="d-flex justify-content-center h-100">
                    <div class="p-2 align-self-center">IFTS</div>
                </div>
            </a>
        </div>
    </div>
</div>