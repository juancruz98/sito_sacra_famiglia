<div class="container-fluid grey pt-100 pb-100">
    <div class="row h-100 justify-content-center">
        <div class="col-10 col-md-6 col-lg-5 align-self-center text-center">
            <h1 class="text-white">are you experienced?</h1>
            <h4 class="text-white">la scuola raccontata dai ragazzi</h4>
            <div class="video-container mt-4">
                <iframe src="https://www.youtube.com/embed/klx_xgPm1U4" frameBorder="0"></iframe>
            </div>
            <div class="video-container mt-4">
                <iframe src="https://www.youtube.com/embed/ji3p7mV72Bo" frameBorder="0"></iframe>
            </div>
            <div class="video-container mt-4">
                <iframe src="https://www.youtube.com/embed/POsEumzRnCY" frameBorder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid font-20">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
            <div class="text-justify">
                <p>
                    Il progetto “Are you experienced?” è un omaggio al rock, ai nostri studenti, alla loro capacità
                    di abbattere ogni difficoltà con disarmante semplicità. Le loro storie, i loro racconti, a
                    dimostrazione di un mondo che ci mette in discussione ad ogni rotazione.
                </p>
            </div>

        </div>
    </div>
</div>