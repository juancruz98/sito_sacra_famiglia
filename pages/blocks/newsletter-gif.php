<div class="container-fluid gif-block-colors h-500">
    <div class="row h-100 justify-content-center relative-pos">
        <div class="grain-background"></div>
        <div class=" col-11 col-md-8 align-self-center">
            <p class="l-spacing-2 font-18">Rimaniamo in contatto</p>
            <p>rimani aggiornato su tutte le nostre iniziative</p>
            <form id="form-newsletter">
                <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Email" name="email" required>
                    <input type="hidden" name="oggetto_mail" value="Nuovo iscritto alle newsletter">
                    <input type="hidden" name="form_type" value="newsletter">
                    <input type="hidden" class="g-recaptcha-response" name="g-recaptcha-response">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-red aquamarine-hover font-18" onclick="sendForm('#form-newsletter')">Iscriviti</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>