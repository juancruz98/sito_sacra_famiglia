<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<!-- fonts -->
<link href="../fonts/roboto.css" rel="stylesheet">
<link href="../fonts/akidenz.css" rel="stylesheet">
<!-- custom css -->
<link rel="stylesheet" href="../styles/style.css">
<link rel="stylesheet" href="../styles/toast.css">
<!-- Calendly link widget begin -->
<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">

<!-- Calendly link widget end -->
<link rel="icon" type="image/png" href="../images/favicon.png" />
<title>Fondazione Sacra Famiglia</title>