<nav class="navbar navbar-expand-lg navbar-dark dark py-4 fixed-top">
    <div class="container">
        <a class="navbar-brand" href="a"> <img src="../images/logo_bianco.png" height="30" class="" alt=""></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <button class="navbar-toggler third-button" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
            <div class="animated-icon3"><span></span><span></span><span></span></div>
        </button>


        <div class="collapse navbar-collapse" id="navbar-main">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown mx-0 px-lg-2">
                    <a class="nav-link p-2 dropdown-toggle mt-4 mt-lg-0" href="#" id="corsi" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        CORSI
                    </a>
                    <div class="dropdown-menu dropdown-dark dark pt-0 pt-lg-3" aria-labelledby="corsi">
                        <a class="dropdown-item py-3" href="dopo-le-medie">DOPO LE MEDIE</a>
                        <a class="dropdown-item py-3" target="_blank" href="https://www.landpage.co/56c1a2fc-a19c-11ea-9280-f2105ecc4ec9">IFTS POST DIPLOMA</a>
                        <!-- <a class="dropdown-item py-3 disabled" href="#">FORMAZIONE CONTINUA</a> -->
                        <a class="dropdown-item py-3" href="apprendistato">APPRENDISTATO</a>
                        <a class="dropdown-item py-3" href="master" target="_blank">MASTER</a>
                        <!-- <a class="dropdown-item py-3" href="#">SERVIZI AL LAVORO</a>
                          <a class="dropdown-item py-3" href="#">CORSI PER AZIENDE</a> -->
                    </div>
                </li>
                <li class="nav-item dropdown mx-0 px-lg-2">
                    <a class="nav-link p-2 dropdown-toggle" href="#" id="progetti" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        PROGETTI
                    </a>
                    <div class="dropdown-menu dropdown-dark dark pt-0 pt-lg-3" aria-labelledby="progetti">
                        <a class="dropdown-item py-3" href="academy">ACADEMY</a>
                        <a class="dropdown-item py-3" href="dopo-le-medie#personalizzazione">PERSONALIZZAZIONE</a>
                        <!-- <a class="dropdown-item py-3" href="#">PARTNER</a> -->
                        <a class="dropdown-item py-3" href="experienced">EXPERIENCED</a>
                    </div>
                </li>
                <li class="nav-item dropdown mx-0 px-lg-2">
                    <a class="nav-link p-2 dropdown-toggle" href="#" id="about" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ABOUT
                    </a>
                    <div class="dropdown-menu dropdown-dark dark pt-0 pt-lg-3" aria-labelledby="about">
                        <a class="dropdown-item py-3" href="about">MISSION</a>
                        <a class="dropdown-item py-3" href="about#sede">SEDE</a>
                        <a class="dropdown-item py-3" href="about#qualita">QUALITÀ</a>
                    </div>
                </li>
                <li class="nav-item mx-0 px-lg-2">
                    <a class="nav-link p-2" href="contatti" id="contatti">CONTATTI</a>
                </li>
                <li class="nav-item dropdown mx-0 px-lg-2">
                    <a class="nav-link p-2 dropdown-toggle" href="#" id="family" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        FAMILY AREA
                    </a>
                    <div class="dropdown-menu dropdown-dark dark pt-0 pt-lg-3" aria-labelledby="family">
                        <a class="dropdown-item py-3" href="https://nuvola.madisoft.it/login" target="_blank">REGISTRO online</a>
                    </div>
                </li>
            </ul>
            <div class="ml-0 ml-lg-5 mt-4 mt-lg-0" action="open-day"> 
                <!-- tolto classe form-inline -->
                <button type="button"  onclick="window.location.href='open-day'"  class="btn navbar-btn btn-grey white-hover font-14 ml-2 ml-lg-0 px-3 py-2">OPEN DAY
                </button>
            </div>
        </div>
    </div>
</nav>

<?php if (isset($select)) : ?>
    <style>
        #<?= $select ?> {
            color: white !important;
        }
    </style>
<?php endif; ?>