<div class="container-fluid font-22">
    <div class="row justify-content-center">
        <div class="col-10 col-md-6 col-lg-5 align-self-center text-center mt-100 mb-100">
            <p class="font-weight-bold">la nostra offerta formativa<br>in 30 secondi</p>
            <div class="video-container mt-4">
                <iframe src="https://www.youtube.com/embed/mP2ubGUAMVc" frameBorder="0"></iframe>
            </div>
            <div class="mt-5">
                <img src="../images/banner_colors.png" class="" alt="" style="width: 50%;">
            </div>
        </div>
    </div>
</div>