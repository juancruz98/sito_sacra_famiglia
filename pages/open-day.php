<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php include('blocks/nav.php'); ?>
    <div class="container-fluid ">
        <div class="row h-100 justify-content-center py-5">
            <div class="col-11 col-md-5 align-self-center m-2">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/EtfqNwH9oss?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameBorder="0"></iframe>
                </div>
            </div>
            <div class=" col-11 col-md-5 align-self-center">
                <p class="l-spacing-2 font-22 text-center text-lg-left mt-4 mt-lg-0">Benvenuti!</p>
                <div class="wrap-left-btn d-none d-lg-block">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
                <div class="wrap-btn d-lg-none">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
                <p class="l-spacing-2 font-22 text-center text-lg-left mt-4 mt-lg-0">Are you a foreign student? Contact Us!</p>
            </div>
        </div>
    </div>

    <div class="container-fluid font-22 dark">
        <div class="row justify-content-center">
            <div class="col-8 col-sm-6 col-lg-4 align-self-center text-center mt-100 mb-100">
                <div>
                    <h2 class="text-white">Prenota il tuo <br>virtual-day a scuola</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12  dark">
        <!-- Calendly inline widget begin -->
        <div class="mx-auto calendly-inline-widget pb-5" data-url="https://calendly.com/sacra-famiglia/open-day-1?hide_event_type_details=1&hide_gdpr_banner=1&primary_color=05c1a7" style="max-width:500px; height:800px;"></div>
        <!-- Calendly inline widget end -->
    </div>
    <?php
    $light_theme = true;
    include('blocks/virtual-tour.php')
    ?>
    <div class="container-fluid orange">
        <div class="row h-100 justify-content-center py-5">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/HB9ZjBQGoew" frameBorder="0"></iframe>
                </div>
            </div>
            <div class=" col-11 col-md-5 align-self-center text-white">
                <p class="mt-5 mt-lg-0 l-spacing-2 font-22 text-center font-weight-bold">Operatore addetto ai servizi di Vendita</p>
                <div class="wrap-left-btn">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid green">
        <div class="row h-100 justify-content-center py-5">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/3yVCbgUTpOA" frameBorder="0"></iframe>
                </div>
            </div>
            <div class=" col-11 col-md-5 align-self-center text-white">
                <p class="mt-5 mt-lg-0 l-spacing-2 font-22 text-center font-weight-bold">Operatore Agricolo</p>
                <div class="wrap-left-btn">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid purple">
        <div class="row h-100 justify-content-center py-5">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/xwa1szPoquI" frameBorder="0"></iframe>
                </div>
            </div>
            <div class=" col-11 col-md-5 align-self-center text-white">
                <p class="mt-5 mt-lg-0 l-spacing-2 font-22 text-center font-weight-bold">Operatore dell'Abbigliamento</p>
                <div class="wrap-left-btn">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid red">
        <div class="row h-100 justify-content-center py-5">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/POsEumzRnCY" frameBorder="0"></iframe>
                </div>
            </div>
            <div class=" col-11 col-md-5 align-self-center text-white">
                <p class="mt-5 mt-lg-0 l-spacing-2 font-22 text-center font-weight-bold">IFTS POST DIPLOMA</p>
                <div class="wrap-left-btn">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="background-color: #1565c0;">
        <div class="row justify-content-center">
            <div class="col-12 align-self-center text-center mt-100 mb-100">
                <div class="wrap-btn">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSe8ZUuRj1YSG8xcBU3Lpn-kBGap21ZSnGUEs7i033ZljO2qZQ/viewform?usp=sf_link" target="_blank">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">PREISCRIVITI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pt-50 pb-50 dark font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-11 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="container justify-content-center">
            <div class=" row justify-content-center h-50">
                <div class="col-12 col-sm-6 col-lg-3 mb-2">
                    <?php include("blocks/sub-blocks/burgo.php") ?>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 mb-2">
                    <?php include("blocks/sub-blocks/capelli.php") ?>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 mb-2">
                    <?php include("blocks/sub-blocks/vitali.php") ?>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 mb-2">
                    <?php include("blocks/sub-blocks/piersanti.php") ?>
                </div>
            </div>
        </div>
    </div>
    <?php include('blocks/sezione-experienced.php'); ?>
    <div class="container-fluid" style="background-color: #1565c0;">
        <div class="row justify-content-center">
            <div class="col-12 align-self-center text-center mt-100 mb-100">
                <div class="wrap-btn">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSdrvhhh1pMddbXI6IG8qWWrsZXittVX2BZXpfVPg7HEdnqIgA/viewform?vc=0&c=0&w=1&flr=0&gxids=7628" target="_blank" rel="noopener noreferrer">
                        <button class="btn-white button-anim black-hover font-20">DACCI UN FEEDBACK</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php include('blocks/offerta-formativa.php');
    ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-10 col-md-6 align-self-center text-center mt-100 mb-100 font-22">
                <i>
                    <b>
                        “se insegni ai bambini come e perché prendersi cura dei loro giardini scolastici, a loro volta creeranno città sane e resilienti del futuro”.
                    </b>
                </i>
            </div>
        </div>
    </div>
    <?php include('blocks/faq.php'); ?>
    <?php include('blocks/footer.php'); ?>
</body>

</html>