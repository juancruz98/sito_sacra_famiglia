<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'corsi';
    include('blocks/nav.php');
    ?>
    <div class="container-fluid grey h-700">
        <div class="row h-100 justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <h3 class="text-white">Apprendistato</h3>
                <div class="video-container mt-4">
                    <iframe src="https://www.youtube.com/embed/SsB8Wu_zeKY" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <div class="text-justify">
                    <p>
                        <b>Sacra Famiglia</b> si occupa di apprendistato professionalizzante, un contratto di
                        lavoro a tempo indeterminato finalizzato alla formazione e all'occupazione dei giovani tra i 18
                        e i
                        29 anni compresi (o fin dai 17 anni se in possesso di una qualifica professionale).
                    </p>
                    <p>
                        Supportiamo gratuitamente, grazie ai fondi messi a disposizione dalla Regione Lombardia, le
                        aziende
                        nel percorso di inserimento dell’apprendista.
                    </p>
                    <p>
                        I servizi che offriamo alle imprese sono:
                    </p>
                    <ul>
                        <li>Definizione del piano formativo individuale adeguato alle necessità dell'apprendista;</li>
                        <li>Programmazione e valutazione della formazione interna dell'apprendista;</li>
                        <li>Inserimento dell’apprendista nei corsi di formazione di base o su tematiche trasversali
                            previste
                            dal regolamento regionale in materia di apprendistato professionalizzante.</li>
                    </ul>
                </div>
                <div class="wrap-btn mt-5">
                    <a href="contatti">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">CONTATTACI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid grey h-700">
        <div class="row h-100 justify-content-center">
            <div class="col-12 col-md-6 col-lg-5 align-self-center text-center">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/ky_XgXDdBY4" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid font-20">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-4 align-self-center mb-100 mt-100">
                <div class="text-justify">
                    <p class="font-weight-bold">
                        BIG MIKE, LA NUOVA FRONTIERA DELL’APPRENDISTATO
                    </p>
                    <p>
                        “Per quanto mi riguarda, l’occasione dell’apprendistato è nata da un caso fortuito.
                    </p>
                    <p>
                        Quando penso a quest’esperienza penso ad un’occasione di arricchimento e sono convinto che in futuro
                        i miei sforzi verranno premiati.”
                    </p>
                </div>
                <div class="wrap-btn mt-5">
                    <a href="contatti">
                        <button class="btn-white black-border btn-300w button-anim black-hover font-20">CONTATTACI</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pt-50 pb-50 grey font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-10 col-sm-6 align-self-center mb-5" id="meeting">
                <h3>prenota un meeting virtuale con il coordinatore dell'area</h3>
            </div>
        </div>
        <div class="col-12 mx-auto mb-2">
            <?php include("blocks/sub-blocks/donadoni.php") ?>
        </div>
    </div>
    <!-- End Sezione Personalizzazione -->
    <?php include('blocks/footer.php'); ?>
</body>

</html>