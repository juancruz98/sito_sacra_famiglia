<!doctype html>
<html lang="it">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php
    $select = 'about';
    include('blocks/nav.php');
    ?>
    <div class="container-fluid pt-5 mb-5">
        <div class="row h-100 justify-content-center ">
            <div class="col-12 col-md-5 col-lg-4 col-xl-3 align-self-center text-center mb-5 mb-lg-0">
                <h1 class="font-22">Mission</h1>
            </div>
            <div class="col-11 col-md-5 col-lg-4 col-xl-3 align-self-center text-alig-left gif-block-colors px-5 pt-50 pb-50">
                <p>
                    Siamo osservatori del mondo che cambia, lavoriamo con le aziende per avvicinare nuove
                    professionalità al mondo del lavoro e cerchiamo costantemente di valorizzare le abilità di chi
                    lavora.
                </p>
                <p>Ci piace l'evoluzione culturale, l'assetto modernista, ma non perdiamo mai l'accento sui nostri
                    valori di fondo: l'accoglienza e la grande professionalità al servizio di chi ha scelto.</p>
            </div>
        </div>
    </div>
    <?php include('blocks/educazione.php'); ?>
    <?php include('blocks/offerta-formativa.php'); ?>
    <!-- Blocco Virtual Tour -->
    <?php include('blocks/virtual-tour.php'); ?>
    <!-- End Blocco Virtual Tour -->
    <!-- Blocco Qualità -->
    <div class="container-fluid" id="qualita">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-lg-6 align-self-center mt-100 mb-100">
                <h3 class="text-center">Gestione Qualità</h3>
                <br>
                <div class="text-justify font-18">
                    <p>EFP Sacra Famiglia ha implementato un Sistema di Gestione per la Qualità orientato ad ottenere la
                        soddisfazione della utenza attraverso il raggiungimento dei requisiti dell’utente stesso, nel
                        rispetto delle leggi e regolamenti applicabili
                        ai servizi forniti.
                    </p>
                    <p>L’Ente intende perseguire questi obiettivi attraverso:</p>
                    <ul>
                        <i>
                            <li>una continua ed efficace applicazione del Sistema di Gestione per la Qualità;</li>
                            <li>la prevenzione delle situazioni di non conformità;</li>
                            <li>il miglioramento continuo dei processi.</li>
                        </i>
                    </ul>
                    <p>
                        Il sistema di Gestione per la Qualità di EFP Sacra Famiglia è conforme alla norma UNI EN ISO
                        9001:2015 per il campo di applicazione: PROGETTAZIONE ED EROGAZIONE DI SERVIZI FORMATIVI,
                        SERVIZI AL LAVORO E SERVIZI DI ORIENTAMENTO.
                    </p>
                    <p>
                        La Direzione dell’EFP Sacra Famiglia ha definito e documentato la propria Politica per la
                        Qualità, dalla quale derivano gli obiettivi generali della Qualità. Ha stabilito di definire
                        annualmente obiettivi e traguardi specifici per ogni funzione e livello
                        significativo dell’organizzazione.
                        <i>La Direzione assicura che la propria Politica per la Qualità sia compresa, attuata e
                            sostenuta a
                            tutti i livelli dell’organizzazione.</i>
                    </p>
                    <p>
                        Come obiettivo minimo da parte di tutte le funzioni, la Direzione individua il sistematico
                        soddisfacimento dei requisiti stabiliti da Regione Lombardia per l’erogazione dei servizi
                        formativi ed il miglioramento continuo della soddisfazione del utente.
                    </p>
                    <p>
                        EFP Sacra Famiglia adotta il <i>Codice Etico</i> quale “carta dei diritti e doveri fondamentali”
                        attraverso la quale l’Ente individua e chiarisce le proprie responsabilità e gli impegni etici
                        verso i propri stakeholder.
                    </p>
                    Il <i>Codice Etico</i> rappresenta uno strumento adottato in via autonoma e suscettibile di
                    applicazione sul piano generale, ed ha lo scopo di esprimere principi di deontologia aziendale che
                    l’EFP Sacra Famiglia, riconosce come propri
                    e sui quali richiama l’osservanza da parte di tutti i Dipendenti, Amministratori, Consulenti e
                    Partner.</p>
                    <div class="wrap-btn mt-5 mb-3">
                        <a href="../documents/CODICE-ETICO.pdf" target="_blank" download="CODICE-ETICO">
                            <button class="btn-white black-border btn-300w button-anim black-hover font-20">scarica il
                                codice etico</button>
                        </a>
                    </div>
                    <div class="wrap-btn">
                        <a href="../documents/MODELLO-231.pdf" target="_blank" download="MODELLO-231">
                            <button class="btn-white black-border btn-300w button-anim black-hover font-20">legge
                                231</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Blocco Qualità -->
    <!-- Blocco Meeting -->
    <?php include('blocks/meeting-virtuale-full.php'); ?>
    <?php include('blocks/faq.php'); ?>
    <!-- End Blocco Meeting -->
    <?php include('blocks/footer.php'); ?>
</body>

</html>