<!doctype html>
<html lang="en">

<head>
    <?php include('blocks/head.php'); ?>
</head>

<body>
    <?php include('blocks/nav.php'); ?>
    <div class="container-fluid font-22">
        <div class="row justify-content-center">
            <div class="col-8 col-sm-6 col-lg-4 align-self-center text-center mt-100 mb-50">
                <p>Entra nei nostri spazi</p>
                <p class="font-weight-bold">Virtual Tour</p>
            </div>
        </div>
    </div>
    <div class="container-fluid font-22">
        <div class="row justify-content-center h-500">
            <div class="col-12 col-md-6 h-100 align-self-center text-center grey-border">
                <div class="d-flex justify-content-center h-100">
                    <div class="p-2 align-self-center">Laboratori</div>
                </div>
            </div>
            <div class="col-12 col-md-6 h-100 align-self-center text-center grey-border">
                <div class="d-flex justify-content-center h-100">
                    <div class="p-2 align-self-center">Laboratori</div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-50 pb-50 black font-22 text-white">
        <div class="row h-50 justify-content-center align-items-center">
            <div class="col-12" id="primo-blocco">
                <div class="mt-50 mb-50">
                    <img src="../images/logo_bianco.png" class="" alt="" style="max-width: 300px;">
                </div>
                <p>da oltre 100 anni impegnati nella formazione</p>
            </div>
        </div>
    </div>
    <?php include('blocks/footer.php'); ?>
</body>

</html>